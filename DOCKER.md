# Docker Images
Images for containers are provided. They contain a LaTeX environment and the laweb
components.

The container needs to have access to your working directory, which has to be mounted into
the container. Example which creates and starts a container, and removes it at the of the job:

```
docker run --rm -v $(pwd):/home/workdir registry.ethz.ch/laweb/laweb:stable tex_source
```

Do not change the path `/home/workdir`! It's hardcoded into the image.

Replace `docker` with `podman` or whatever container runtime you want to use. 

## Docker Setup with WSL
The Docker image works identically on Windows, if you use *Docker Desktop* and the 
Windows subsystem for Linux. Make sure that `/mnt/c/Program Files/Docker/Docker/resources/bin` 
has been added to your path. 

The path to the WSL directory has to be noted in a way which Docker Desktop is able to find.
A path may look like

```
\\\\wsl.localhost\\Ubuntu-20.04\\home\\myuser\\my_work_dir
```

Type "\\wsl$" in a file explorer window to navigate to your directory and get a path string,
and add double every `\`!
