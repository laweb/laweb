#!/usr/bin/env bash

# Wrapper for container builds, not for interactive usage

if [ "$(pwd)" != "/home/workdir" ]; then
    cd /home/workdir
fi

/usr/local/bin/laweb $1
