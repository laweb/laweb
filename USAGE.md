# Usage

In your preamble, add

```
\usepackage{laweb}
```


## Command Line

Issue

```./laweb <BASENAME>```

Ommit the `.tex` suffix for `<BASENAME>`.

For the command line using Docker images see the [Docker instructions](DOCKER.md).

## laweb2pressbooks: HTML to Wordpress XML Conversion
See [the instructions here](https://gitlab.ethz.ch/laweb/laweb2pressbooks).
