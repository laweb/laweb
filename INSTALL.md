# Installation
## Laweb: LaTeX to HTML Conversion
Copy the files

 - `laweb`
 - `laweb.sty`
 - `laweb.cfg`

to the directory containing your LaTeX main file. 

As dependencies, install the packages

 - tex4ht
 - tikz

and a comprehensive installation of LaTeX. I.e. on Fedora this will prepare the environment:

```
dnf install -y texlive-tikz-dependency texlive-tex4ht texlive-scheme-full texlive-ifluatex
```

If you have no customizable LaTeX environment, you may use the [Docker image](DOCKER.md)

## laweb2pressbooks: HTML to Wordpress XML Conversion
See [the instructions here](https://gitlab.ethz.ch/laweb/laweb2pressbooks).
