# *Laweb*: LaTeX to Pressbooks Conversion

Publish LaTeX documents in *Pressbooks*, a Wordpress plugin.

---
![Screenshot of Pressbooks publication created with Laweb](static/Screenshot_Pressbooks-01.png)

---

This is a tool designed to transform a LaTeX source into a HTML file which
can be
[converted to a Wordpress import XML file](https://gitlab.ethz.ch/laweb/laweb2pressbooks).

In the background, it uses `tex4ht` for conversion. 

*Laweb* is licensed under the [GNU General Public License, version 3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) or later

# Development and Roadmap
The further development of *Laweb* depends on your input. Whether you can contribute some software project financing, have skilled developers in your group which could devote some work to the project or you may even think about an [Innvovedum project grant application](https://ethz.ch/en/the-eth-zurich/education/innovedum/innovedum-fund.html). 

# Technical information
## Download and Installation

See [INSTALL](INSTALL.md) for installation information.

## Usage

See [USAGE](USAGE.md) for usage instructions.

## Do Not Edit in Pressbooks
It is important to note that the content imported to Pressbooks **may not be edited in Pressbooks**. The editor(s) integrated in Pressbooks will destroy the HTML when saving modifications. The book has to be deleted and imported again to recover.

# Overview
*Laweb* combines the unmatched presentation of scientific documents with the publication abilities of Wordpress. 

## Why use *Laweb*?
While it is possible to simply convert a LaTeX document into HTML and put it on a static webserver, this approach
has some drawbacks. Some features of *Laweb* are:

- *Laweb* uses MathML and Mathjax for the rendering of mathematical equiations, resulting in a near to optimum presentation in different browsers
- usually, a conversion to HTML ends up in a single HTML document. Think of a document consisting of hundreds of pages! *Laweb* splits parts and sections into single pages and creates the navigation structure for Pressbooks.
- Images are converted to inline data embedded in the HTML. Losing references to images is impossible.
- *Laweb* ensures that link references remain functional and consistent across split up pages.
- *Laweb* converts links to http://ggbm/at/MATERIAL_ID to interactive [Geogebra](https://www.geogebra.org/) applets

## Components
*Laweb* consists of two components:

- *laweb*: LaTeX style and helper scripts to convert LaTeX source code to HTML
- *laweb2pressbooks*: Converter from HTML to Wordpress XML

The LaTeX styles add information required by the XML conversion. The resulting XML contains each LaTeX *part* and *section* separated, resulting in a structured Wordpress publication.

Additionally, a Pressbook template with the ability to disable the `wpautop` function in Wordpress is required. Otherwise, the rendering engine of Wordpress will eliminate vital parts of the HTML during rendering.

## Example
See [the walkthrough](https://gitlab.ethz.ch/laweb/laweb/-/wikis/walkthrough/1-Example-Start) for a an example *Laweb* conversion from source to Pressbooks.




