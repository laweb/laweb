FROM registry.ethz.ch/laweb/latex-prebuild:f38
LABEL maintainer="Bengt Giger <bgiger@ethz.ch>"

COPY laweb.sty /usr/share/texlive/texmf-local/tex/latex/laweb.sty
COPY laweb.cfg /usr/share/texlive/texmf-local/tex/latex/laweb.cfg
RUN mktexlsr /usr/share/texlive/texmf-local
COPY laweb /usr/local/bin/laweb
COPY laweb-wrapper.sh /usr/local/bin/laweb-wrapper
RUN chmod a+rx /usr/local/bin/laweb-wrapper

WORKDIR /home/workdir

VOLUME [ "/home/workdir" ]

ENTRYPOINT ["/usr/local/bin/laweb"]

